import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Poll } from '../../../models/poll.model';
import { MockingService } from '../../../services/mocking.service';
import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'app-navigation',
	templateUrl: './navigation.component.html',
	styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
	public _pollsAvailables: Observable<Poll[]> = this.mockingService.pollsAvailables;
	public devModeEnabled = !environment.production;
	@Input() public appTitle = 'FramaDate Funky';
	@Input() public appLogo: string;
	public environment = environment;
	mobileMenuVisible: boolean;

	constructor(private mockingService: MockingService) {}

	public ngOnInit(): void {}
}
