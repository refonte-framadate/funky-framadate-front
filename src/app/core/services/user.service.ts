import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { UserRole } from '../enums/user-role.enum';
import { Owner } from '../models/owner.model';
import { ApiService } from './api.service';

@Injectable({
	providedIn: 'root',
})
export class UserService {
	public anonymous: Owner = new Owner('', '', [], UserRole.ANONYMOUS);

	private _user: BehaviorSubject<Owner> = new BehaviorSubject<Owner>(this.anonymous);
	public readonly user: Observable<Owner> = this._user.asObservable();

	constructor(private apiService: ApiService) {}

	public updateUser(user: Owner): void {
		this._user.next(user);
	}

	public getCurrentUser(): Owner {
		return this._user.getValue();
	}

	public isCurrentUserIdentifiable(): boolean {
		return this._user.getValue()?.pseudo ? true : false;
	}

	public async getUserPolls(): Promise<void> {
		const currentUser: Owner = this._user.getValue();
		currentUser.polls = await this.apiService.findMyPollsByEmail(currentUser.email);
		this.updateUser(currentUser);
	}

	public async sendEmailToUserTheListOfItsPolls(): Promise<void> {
		await this.apiService.findMyPollsByEmail(this._user.getValue().email);
	}
}
