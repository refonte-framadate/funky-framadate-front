import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Answer } from '../../../core/enums/answer.enum';
import { Choice } from '../../../core/models/choice.model';
import { Poll } from '../../../core/models/poll.model';

@Component({
	selector: 'app-choice-details',
	templateUrl: './choice-details.component.html',
	styleUrls: ['./choice-details.component.scss'],
})
export class ChoiceDetailsComponent implements OnInit {
	public answerEnum = Answer;

	constructor(
		public dialogRef: MatDialogRef<ChoiceDetailsComponent>,
		@Inject(MAT_DIALOG_DATA) public choice: Choice,
		@Inject(MAT_DIALOG_DATA) public poll: Poll
	) {}

	ngOnInit(): void {}

	public closeDialog(): void {
		this.dialogRef.close();
	}
}
