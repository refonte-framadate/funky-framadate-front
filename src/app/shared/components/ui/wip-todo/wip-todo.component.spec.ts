import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WipTodoComponent } from './wip-todo.component';

describe('WipTodoComponent', () => {
	let component: WipTodoComponent;
	let fixture: ComponentFixture<WipTodoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WipTodoComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WipTodoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
