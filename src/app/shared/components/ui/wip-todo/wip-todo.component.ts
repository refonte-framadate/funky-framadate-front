import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'app-wip-todo',
	templateUrl: './wip-todo.component.html',
	styleUrls: ['./wip-todo.component.scss'],
})
export class WipTodoComponent implements OnInit {
	env: any = environment;
	constructor() {}

	ngOnInit(): void {}
}
