import { Component, Input, OnInit } from '@angular/core';
import { Poll } from '../../../core/models/poll.model';
import { Answer } from '../../../core/enums/answer.enum';
import { ModalService } from '../../../core/services/modal.service';
import { StorageService } from '../../../core/services/storage.service';
import { Choice, ChoiceGroup } from '../../../core/models/choice.model';

@Component({
	selector: 'app-poll-results-dinum',
	templateUrl: './poll-results-dinum.component.html',
	styleUrls: ['./poll-results-dinum.component.scss'],
})
export class PollResultsDinumComponent implements OnInit {
	@Input() public poll: Poll;
	@Input() public detailledDisplay: boolean = false;
	constructor(private modalService: ModalService, private storageService: StorageService) {}

	ngOnInit(): void {}

	toggleAnswer(choice_id: number, value: string) {
		this.storageService.toggleAnswer(choice_id, value);
	}

	showAsDate(date_string: string) {
		return new Date(date_string);
	}

	/**
	 * calculer localement le nombre max de réponses pour un choix parmi ses oui peut-être et non.
	 * c'est différent que de calculer ce maximum par rapport à tous les choix possibles
	 * @param c
	 */
	getMax(c: Choice) {
		return Math.max(c[Answer.YES].count, c[Answer.MAYBE].count, c[Answer.NO].count);
	}
}
