import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MD5, enc } from 'crypto-js';

@Component({
	selector: 'app-password-prompt',
	templateUrl: './password-prompt.component.html',
	styleUrls: ['./password-prompt.component.scss'],
})
export class PasswordPromptComponent implements OnInit {
	password = 'le pass woute woute';
	method = 'md5';
	display_pass = true;
	custom_url = '';
	password_ciphered: string;

	constructor(private router: Router, private _Activatedroute: ActivatedRoute) {}

	ngOnInit(): void {
		this._Activatedroute.paramMap.subscribe((params: ParamMap) => {
			this.custom_url = params.get('custom_url');
		});
	}

	cipherPass() {
		this.password_ciphered = MD5(this.password).toString(enc.Hex);
		this.password_ciphered = MD5(this.password_ciphered).toString(enc.Hex);
		return this.password_ciphered;
	}

	redirectToConsultationPage() {
		this.router.navigate(['/poll/' + this.custom_url + '/consultation/secure/' + this.password_ciphered]);
	}
}
