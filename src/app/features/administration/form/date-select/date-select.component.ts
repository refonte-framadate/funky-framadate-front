import { ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { FormArray, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { UuidService } from '../../../../core/services/uuid.service';
import { ToastService } from '../../../../core/services/toast.service';
import { PollService } from '../../../../core/services/poll.service';
import { DateUtilitiesService } from '../../../../core/services/date.utilities.service';
import { ApiService } from '../../../../core/services/api.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { DateChoice, defaultTimeOfDay, TimeSlices } from '../../../../../../mocks/old-stuff/config/defaultConfigs';

import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../../../../core/services/storage.service';
import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'app-date-select',
	templateUrl: './date-select.component.html',
	styleUrls: ['./date-select.component.scss'],
})
export class DateSelectComponent implements OnInit {
	@Input()
	form: UntypedFormGroup;

	displaySeveralHoursChoice = true;
	allowSeveralHours = true;
	today = new Date();
	endDateInterval: string;

	intervalDaysDefault = environment.interval_days_default;
	dateChoices: DateChoice[] = []; // sets of dateChoices as strings, config to set identical time for dateChoices in a special dateChoices poll
	timeSlices: TimeSlices[] = []; // ranges of time expressed as strings

	selectionKind = 'range';
	display: any;

	constructor(
		private fb: UntypedFormBuilder,
		private cd: ChangeDetectorRef,
		private uuidService: UuidService,
		private toastService: ToastService,
		private pollService: PollService,
		private apiService: ApiService,
		private storageService: StorageService,
		private router: Router,
		private translateService: TranslateService,
		@Inject(DOCUMENT) private document: any
	) {
		this.dateChoices = this.storageService.dateChoices;
		this.timeSlices = this.storageService.timeSlices;
	}

	ngOnInit(): void {}

	/**
	 * change time spans
	 */
	addTime() {
		this.timeSlices.push({
			literal: '',
		});
	}

	removeAllTimes() {
		this.timeSlices = [];
		this.dateChoices.map((elem) => (elem.timeSlices = []));

		this.toastService.display('périodes horaires vidées');
	}

	resetTimes(slices = Object.create(defaultTimeOfDay)) {
		this.timeSlices = slices;
		this.dateChoices.map((elem) => (elem.timeSlices = Object.create(slices)));
		this.toastService.display('périodes horaires réinitialisées');
	}

	openSimple() {
		this.display = !this.display;
	}
}
