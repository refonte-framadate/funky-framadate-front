# What about Framadate v1?
source: https://framagit.org/framasoft/framadate/framadate/-/issues/545
TL;DR: The project is now in maintenance mode. Follow https://framagit.org/framasoft/framadate/funky-framadate-front and https://framagit.org/tykayn/date-poll-api
So I'm going to give a quick update on the status of this project.
History


2009/2010: The university of Strasbourg develops STUdS

May 2011: a Framasoft member forks STUdS into "opensondage" and cleans the code. They installs it for our own internal usage.

June 2011: Framasoft works again on STUdS/Opensondage, make it more "sexy" (mostly frontend work), the service is accessible publicly at https://framadate.org


2014: the project is transferred to an employee of Framasoft
There was a lot of work on the a11y front https://framablog.org/2014/11/26/framadate-nouvelle-version-accessible/ (in french)

2015: the project is handed to volunteers
version 1.0 came out https://framablog.org/2016/10/17/framadate-passage-en-v1-happy-hour-pour-tout-le-monde/ (in french)

2016: these volunteers are no longer active

2018: It's the beginning of multiple tentatives to rethink Framadate, including some work by a volunteer designer who produced some mockups.

2019: A physical reunion with myself and some volunteers (https://www.codeursenliberté.fr/entreprise/contribution_au_libre/ in french) produces what's now the 1.2.0 alpha release, but without further work, it's left abandoned (this is now the develop branch).
Around the same time, the poll table gets a UI improvement, which is hotpatched on framadate.org but still not merged here: !412


2016-2021: small contributions from Framasoft/myself and from some volunteers, including some students, bring a few minor versions.
A few UX improvements are added, but mostly frontend stuff.
The one thing that seems to survive from the multiple tentatives to reboot Framadate is a rewrite in Angular and Symfony: https://framagit.org/framasoft/framadate/funky-framadate-front and https://framagit.org/tykayn/date-poll-api


2021: A french organization (not sure if I can say who) wants to support Framadate and seems to plan to work on the project I've just mentioned above.

Current status
What important here is that we lie on a huge pile of technical dept (more or less well written PHP from ten years ago), because no one - including Framasoft and myself - can find time for Framadate.
Framasoft has currently a lot of ongoing projects, and does not wish to try to maintain this one any longer (not that it was done).
I could give maintainer rights to some people like @sbernhard who seems to have sent a number of MRs and seems interested in Framadate, but given the state of the project, it's probably better to fork it (it would also remove the frama part in the project name, which we would like to get rid of anyway). If you do, please inform us so that we may link to your own project.
Or you can also follow and maybe contribute to this rewrite https://framagit.org/framasoft/framadate/funky-framadate-front and https://framagit.org/tykayn/date-poll-api :)
If it become usable and stable enough, we'll consider it to replace the service at https://framadate.org
I'm putting the project in maintenance mode for now, meaning we only commit to release security fixes in the foreseeable future.
I hope you can understand our position.
Thomas, for Framasoft.
